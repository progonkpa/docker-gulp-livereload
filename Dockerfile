FROM node:12.12.0-alpine

MAINTAINER progonkpa <progonkpa@telenet.be>

ARG SRC_DIR_GUEST
ENV SRC_DIR_GUEST $SRC_DIR_GUEST

RUN apk add dos2unix
RUN npm install --global gulp-cli

COPY setup-node.sh /setup-node.sh

# Converts line seperators from Windows to UNIX. After that the package is not needed anymore and removed.
RUN dos2unix /setup-node.sh
RUN apk del dos2unix
RUN rm -rf /var/lib/apt/lists/*

CMD ["/bin/sh", "/setup-node.sh"]

WORKDIR $SRC_DIR_GUEST

# Livereload.
EXPOSE 35729
