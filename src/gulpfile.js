let {parallel, watch, src} = require('gulp');
let connect = require('gulp-connect');

function serverLivereload(cb) {
    connect.server({livereload: true});
    cb();
}

function watchTemplates(cb) {
    watch('*.html', {interval: 1000, usePolling: true}, html);
    cb()
}

function html(cb) {
    // Point src() to 1 file to keep unnecessary streaming overhead to a minimum.
    src('index.html').pipe(connect.reload());
    cb();
}

exports.default = parallel(serverLivereload, watchTemplates);
