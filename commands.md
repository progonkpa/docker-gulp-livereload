Build image.  
`docker build --build-arg SRC_DIR_GUEST=/var/www/src -t progonkpa/gulp-livereload:latest .`
  
Start a container using the previously built image.  
`docker run -v /$(pwd)/src:/var/www/src -e "SRC_DIR_GUEST=/var/www/src" --name progonkpa_gulp-livereload progonkpa/gulp-livereload:latest sh`  
`docker run -v /$(pwd)/src:/var/www/src -e "SRC_DIR_GUEST=/var/www/src" --name progonkpa_gulp-livereload progonkpa/gulp-livereload:latest gulp`
  
Enter a shell inside the running container.  
The 'name' directive matches partial names.  
`docker exec -it $(docker ps -aqf "name=gulp-livereload") sh`  

Show logs of the gulp-livereload container (or in PhpStorm > Views > Tools > Services).  
`docker logs $(docker ps -aqf "name=gulp-livereload")` 
